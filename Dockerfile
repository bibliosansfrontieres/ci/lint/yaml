FROM alpine:3.19

WORKDIR /yaml

RUN apk add --no-cache yamllint=1.33.0-r0

COPY entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]