#!/bin/sh
cd /yaml

if [ -n "$YAMLLINT_CONFIG_FILE" ]; then
    echo "Using $YAMLLINT_CONFIG_FILE as yamllint configuration"
    lint_command="yamllint -c $YAMLLINT_CONFIG_FILE"
else
    echo "Using default yamllint configuration"
    lint_command="yamllint"
fi

if [ -n "$YAML_FILES_TO_LINT" ]; then
    echo "Linting specific files using YAML_FILES_TO_LINT variable"
    for file in $YAML_FILES_TO_LINT; do
        echo "Linting $file"
        $lint_command "$file"
    done
else
    echo "Linting all yaml files found"
    find . -type f \( -name '*.yaml' -o -name '*.yml' \) \
        -print0 | while IFS= read -r -d '' file; do
        echo "Linting $file"
        $lint_command "$file"
    done
fi