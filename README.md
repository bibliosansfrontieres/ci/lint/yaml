# CI : Lint YAML

Will lint every .yml or .yaml files in project using Python package yamllint.

By default, it will use default yamllint configuration.

[yamllint documentation](https://yamllint.readthedocs.io/en/stable/index.html)

## Usage

```
include:
  - project: 'bibliosansfrontieres/ci/lint/yaml'
    file: 'template.yml'
```

## Override variables

|Variable|Description|Default|Required|
|-|-|-|-|
|YAMLLINT_CONFIG_FILE|Config file to use for yamllint| |no|
|YAML_FILES_TO_LINT|Specific yaml files you want to lint, take all yaml files by default| |no|

## Choose stage

By default, `lint_yaml` uses `lint` stage.

You can override this by adding following code to your `.gitlab-ci.yml` :

```
lint_yaml:
  stage: THE_STAGE_YOU_WANT_TO_USE
```

## Use locally

Copy compose.yml to your project.

Then run :

```
docker compose up --pull always
```

You can also rename `compose.yml` to any name you prefer `ci.yml` for example and then run :

```
docker compose -f ci.yml up --pull always
```
